const { add } = require("./task2")
const array1 = [1, 2, 3, 4];
test('adds 1 + 2 to equal 3', () => {
  expect(add(array1)).toBe(10);
});