#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")

program
    .command('obfuscate', 'Obfuscate Text')
    .argument('<text>', 'Text that need to check')
    .action(({ logger, args }) => {
        String.prototype.obfuscate = function () {
            var bytes = [];
            for (var i = 0; i < this.length; i++) {
                bytes.push(this.charCodeAt(i).toString(16));
            }
            return bytes.join('$');
        }

        logger.info(args.text.obfuscate())
        
    })

program.run()

