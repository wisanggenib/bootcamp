#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")

program
    .command('palindrome', 'Check Word Palindrom or not')
    .argument('text', 'Text that need to check')
    .action(({ logger, args }) => {
        let reverse = args.text.replace(/[^A-Z0-9]/ig, "").split("").reverse().join("").toLowerCase()
        let val = args.text.replace(/[^A-Z0-9]/ig, "").toLowerCase()
        if (reverse === val) {
            console.log('String : ' + args.text)
            logger.info('Is Palindrome ? Yes')
        }
        else {
            console.log('String : ' + args.text)
            logger.info('Is Palindrome ? No')
        }

    })

program.run()


