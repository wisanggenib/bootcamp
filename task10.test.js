const axios = require('axios');

const sendGetRequest = async () => {
    try {
        const resp = await axios.get("https://jsonplaceholder.typicode.com/posts");
        return resp.data.length;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};

//create class fetcher
Post = async (url, data) => {
    try {
        const resp = await axios.post(url, data);
        //console.log(resp.data.data)
        return resp.data.data
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};


//json Data for param 
let dataJson = { "id": 30, "name": "Someone" };

//execute

it('works with async/await', async () => {
    //expect.assertions(1);
    const data = await Post("https://httpbin.org/post", dataJson);
    expect(data).toEqual(JSON.stringify(dataJson));
});

it('must be 100 datas', async () => {
    const data = await sendGetRequest()
    expect(data).toBe(200)
})
