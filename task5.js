#!/usr/bin/env node

const { program } = require("@caporal/core")

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

const number = '0123456789'
const stringg = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
let datas = stringg
let longData = 32

program
    // .command('random', 'Random Number')
    // .argument("text", "free text")
    .option("-l,--letters", "Letter allow or not", { default: false })
    .option("-n,--numbers", "nummber allow or not")
    .option("-o,--lowercase", "lowecase output", { default: false })
    .option("-u,--uppercase", "uppercase output", { default: false })
    .option('-g,--long <amount>', 'length', program.int)
    .action(({ logger, args, options }) => {
        console.log({ args, options })

        const { numbers, letters, lowercase, uppercase } = options

        let word = datas
        if (letters)
            word = stringg
        if (numbers)
            word = number

        if (!letters && !numbers)
            word = stringg + number

        console.log({ word })
        logger.info("Order received: %s", options)
        // console.log('=================================')

        if (lowercase)
            word = word.toLowerCase()

        if (uppercase)
            word = word.toUpperCase()
        // if (options.letters === false) {
        //     datas = ''
        //     datas = datas + number
        // }

        // if (options.numbers === false) {
        //     datas = ''
        //     datas = datas + stringg
        // }

        // if (options.lowercase === true) {
        //  //   datas = ''
        //     let x = datas.toLowerCase()
        //     datas = x
        // }

        // if (options.uppercase === true) {
        //     //   datas = ''
        //        let x = datas.toUpperCase()
        //        datas = x
        //    }

        //  if (options.long){
        //      longData = options.long
        //  }


        //  if (options.numbers === false && options.letters === false)
        // console.log('cant turn off numbers and letters')
        // else
        // console.log("Output : "+ randomString(longData, datas ));

        if (options.long)
            longData = options.long

        let resultString = randomString(longData, word)
        logger.info("result: " + resultString)
    })

program.run()