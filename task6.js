#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")

program
    .command('ip', 'get IP Private')
    .action(({ logger }) => {
        var os = require('os');

        var interfaces = os.networkInterfaces();
        for (var k in interfaces) {
            for (var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if (address.family === 'IPv4' && address.internal === true) {
                    logger.info(address.address);
                }
            }
        }

    })

program.run()
