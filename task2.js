#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")

const { insertOrder } = require("./functions/cart")
/*
program.action(({ logger }) => {
  logger.info("Hello, world!")
})
*/
//penjumlahan
const add = (numbers) => numbers.reduce((accumulator, currentValue) => accumulator + currentValue, 0)

//conect to model to insert new order/data

// logger.info(args.number.reduce(reducer))
program
    .command('add', 'add all number')
    .argument('number...', 'object number')
    .action(({ logger, args }) => {
        // console.log({ args })
        const total = add(args.number)
        logger.info("Total:" + total)

        //insertOrder()
    })

program
    .command('subtract', 'subtract all number')
    .argument('number...', 'object number')
    .action(({ logger, args }) => {
        const reducer = (accumulator, currentValue) => accumulator - currentValue;
        logger.info(args.number.reduce(reducer))
    })

program
    .command('multiply', 'multiply all number')
    .argument('number...', 'object number')
    .action(({ logger, args }) => {
        const reducer = (accumulator, currentValue) => accumulator * currentValue;
        logger.info(args.number.reduce(reducer))
    })

program
    .command('divide', 'divide all number')
    .argument('number...', 'object number')
    .action(({ logger, args }) => {
        const reducer = (accumulator, currentValue) => accumulator / currentValue;
        logger.info(args.number.reduce(reducer))
    })
program.run()


module.exports = { add }