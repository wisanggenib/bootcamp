#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")

  program
    .command('uppercase', 'uppercasting text')
    .argument('text', 'Text Value')
    .action(({ logger, args }) => {
      logger.info(args.text.toUpperCase())
    })

    program
    .command('lowercase', 'uppercasting text')
    .argument('text', 'Text Value')
    .action(({ logger, args }) => {
      logger.info(args.text.toLowerCase())
    })

    program
    .command('capitalize', 'capitalize text')
    .argument('text', 'Text Value')
    .action(({ logger, args }) => {
    logger.info(args.text.trim().toLowerCase().replace(/\w\S*/g, (w) => (w.replace(/^\w/, (c) => c.toUpperCase()))))
})
program.run()