#!/usr/bin/env node
// file: hello-world.js (make the file executable using `chmod +x hello.js`)
const { program } = require("@caporal/core")
const axios = require("axios");
const cheerio = require("cheerio");

getContent = async (url) => {
    axios
        .get(url)
        .then((response) => {
            const html = response.data;
            const $ = cheerio.load(html);

            console.log("")
            //getting title from class movie-info-title and trim the space
            let title = [];
            $('.article__link').each(function (i, e) {
                console.log(title[i] = $(this).text())
                console.log("=============================================")
            });
            
        })
        .catch((error) => {
            console.log(error);
        })
};


program
    .command('headlines', 'Get Headlines From Kompass')
    .action(({ }) => {
        getContent('https://www.kompas.com/')
    })

program.run()
